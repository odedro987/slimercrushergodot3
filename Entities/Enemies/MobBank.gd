extends Node
#MobSpawner
const MOB_SPAWNER = preload("res://Entities/Enemies/MobSpawner.tscn")
#Mob properties
var decisionDelay = 2
var delayRandomizer = 0.5
#Mob consts
const SLIME = preload("res://Entities/Enemies/Slime.tscn")
const BIG_SLIME = preload("res://Entities/Enemies/BigSlime.tscn")
const MINI_SLIME = preload("res://Entities/Enemies/MiniSlime.tscn")
const HUGE_SLIME = preload("res://Entities/Enemies/HugeSlime.tscn")
#Return Mob class by name.
func getMobByName(mob):
	if(mob == "SlimeBig"):
		return BIG_SLIME.instance()
	elif(mob == "SlimeMini"):
		return MINI_SLIME.instance()
	elif(mob == "SlimeHuge"):
		return HUGE_SLIME.instance()
	elif("Slime" in mob):
		return SLIME.instance()
#Return new MobSpawner.
func getMobSpawner():
	return MOB_SPAWNER.instance()