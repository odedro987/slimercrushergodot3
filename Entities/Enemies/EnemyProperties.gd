extends Node

var enemyProperties = {
	"SlimePink" 		:	{
								"Name"             : "Pink Slime",
								"Sprite"           : "res://Assets/Enemies/SlimePink.png",
								"BaseHealth"       : 40,
								"BaseSpeed"        : 20,
								"BaseDamage"       : 3,
								"KnockbackResist"  : 0,
								"XPAmount"         : 10,
								"BehaviorType"     : "Passive",
								"Level"            : 1
							},
	"SlimeBlue" 		:	{
								"Name"             : "Blue Slime",
								"Sprite"           : "res://Assets/Enemies/SlimeBlue.png",
								"BaseHealth"       : 70,
								"BaseSpeed"        : 35,
								"BaseDamage"       : 7,
								"KnockbackResist"  : 0,
								"XPAmount"         : 20,
								"BehaviorType"     : "Follow",
								"Level"            : 2
							},
	"SlimeRed" 			:	{
								"Name"             : "Red Slime",
								"Sprite"           : "res://Assets/Enemies/SlimeRed.png",
								"BaseHealth"       : 60,
								"BaseSpeed"        : 30,
								"BaseDamage"       : 15,
								"KnockbackResist"  : 5,
								"XPAmount"         : 30,
								"BehaviorType"     : "Jumping",
								"Level"            : 3
							},
	"SlimeBig" 			:	{
								"Name"             : "Big Pink Slime",
								"Sprite"           : "res://Assets/Enemies/BigSlime.png",
								"BaseHealth"       : 200,
								"BaseSpeed"        : 20,
								"BaseDamage"       : 30,
								"KnockbackResist"  : 25,
								"XPAmount"         : 100,
								"BehaviorType"     : "Follow",
								"Level"            : 6
							},
	"SlimeHuge" 		:	{
								"Name"             : "Huge Slime",
								"Sprite"           : "res://Assets/Enemies/HugeSlime.png",
								"BaseHealth"       : 600,
								"BaseSpeed"        : 20,
								"BaseDamage"       : 70,
								"KnockbackResist"  : 100,
								"XPAmount"         : 500,
								"BehaviorType"     : "Follow",
								"Level"            : 10
							},
	"SlimeMini" 		:	{
								"Name"             : "Mini Slime",
								"Sprite"           : "res://Assets/Enemies/MiniSlime.png",
								"BaseHealth"       : 5,
								"BaseSpeed"        : 100,
								"BaseDamage"       : 5,
								"KnockbackResist"  : 0,
								"XPAmount"         : 10,
								"BehaviorType"     : "Follow",
								"Level"            : 1
							}
}