extends KinematicBody2D

enum DecisionType {
	IDLE = 0,
	GO_RIGHT = 1,
	GO_LEFT = -1
}

const DECISION_PROBABILITY = [50, 25, 25]
var delay
var timer
var decision
var velocity
var motion
var collision
var normalize
var playerPos
var player
var spriteSize
var shapeMargin
#Damage vars.
var invincible = false
var isDamageDealt
var isAttacking = false
var canDamage = false
var knockbackResistance = Vector2(0, 0)
var direction = -1
var invincibleTime = 0.0
const INVINCIBLE_LIMIT = 0.3
#Slime attributes.
var health
var speed
var damage
var behavior
var xpAmount
var level
var slimeName

var jumpForce = Vector2(200, 150)
var knockbackForce = Vector2(120, 130)
var canJump = true
const DECELERATION = 300
var isJumping = false
var isCollidingWithPlayer = false
var timeSinceLastJump = 0.0
const TIME_BETWEEN_JUMPS = 1.5

enum States { NORMAL, HURT }
var currentState = 0

func _ready():
	randomize()
	#Starts invisible.
	modulate.a = 0
	#Sets randomized delay.
	delay = MobBank.decisionDelay + rand_range(-MobBank.delayRandomizer, MobBank.delayRandomizer)
	#Init vars.
	timer = 0
	velocity = Vector2(0, 0)
	decision = DecisionType.IDLE
	player = get_tree().get_root().get_node("PlayState/Player")
	#Starts Bounce animation.
	get_node("AnimationPlayer").play("Bounce")
	#Update function.
	set_physics_process(true)
#Loads information from EnemyProperties by type.
func setType(type):
	self.health   = EnemyProperties.enemyProperties[type]["BaseHealth"] * GameState.Difficulty.mobHealthMultiplier
	self.speed    = EnemyProperties.enemyProperties[type]["BaseSpeed"] * GameState.Difficulty.mobSpeedMultiplier
	self.damage   = EnemyProperties.enemyProperties[type]["BaseDamage"] * GameState.Difficulty.mobDamageMultiplier
	self.behavior = EnemyProperties.enemyProperties[type]["BehaviorType"]
	self.xpAmount = EnemyProperties.enemyProperties[type]["XPAmount"] * GameState.Difficulty.mobEXPMultiplier
	var kbResist = EnemyProperties.enemyProperties[type]["KnockbackResist"]
	self.knockbackResistance = Vector2(kbResist, kbResist)
	get_node("Sprite").texture = load(EnemyProperties.enemyProperties[type]["Sprite"])
	spriteSize = Vector2(0, 0)
	spriteSize.x = get_node("Sprite").texture.get_size().x / get_node("Sprite").hframes
	spriteSize.y = get_node("Sprite").texture.get_size().y / get_node("Sprite").vframes
	shapeMargin = get_node("CollisionShape2D").shape.extents.x + get_node("CollisionShape2D").position.x
	self.slimeName = EnemyProperties.enemyProperties[type]["Name"]
	self.level     = EnemyProperties.enemyProperties[type]["Level"]
func _physics_process(delta):
	#Checks invincibility.
	invincibilityTimer(delta)
	if(!isDamageDealt):
		decisionTimer(delta)
		#States:
		behave(behavior, delta)
		#Checks walkBounds and reverse direction.
		checkCanWalkInDirection()
		#Search for collisions.
		checkCollisions()
	else:
		#Applys gravity.
		velocity.y += GlobalVariables.GRAVITY * delta
		#Calcualtes the direction of the knockback force(the sign +/-).
		var hitDirection = 0
		if(velocity.x != 0): hitDirection = velocity.x / abs(velocity.x)
		#Decelerates to the opposite direction of the hit.
		velocity.x = 0 if abs(velocity.x) - 100 * delta < 0 else velocity.x + 100 * delta * -hitDirection
	#Moves the slime.
	motion = velocity * delta
	collision = move_and_collide(motion)
	#Checks onGround
	checkOnGround(delta)
#Behave by behavior type.
func behave(behavior, delta):
	if(self.behavior == "Passive"):
		behavePassive()
	elif(self.behavior == "Follow"):
		behaveFollow()
	elif(self.behavior == "Jumping"):
		behaveJumping(delta)
		if(!isJumping):
			behaveFollow()
	else: 
		behavePassive()
#Returns a random DecisionType based on odds upto 100.
func randomizeDecision():
	var random = rand_range(0, 100)
	for i in range(0, DECISION_PROBABILITY.size()):
		if(random <= DECISION_PROBABILITY[i]): return i
		random -= DECISION_PROBABILITY[i]
#Applies knockback.
func knockback(force, newDirection):
	newDirection = newDirection if newDirection != 0 else -direction
	velocity = Vector2(force.x * newDirection, -force.y)
#Decreases health.
func damageEnemy(damageDealt, knockbackForce, newDirection):
	if(!invincible):
		#Sets invincible and isDamageDealt to true.
		invincible = true
		isDamageDealt = true
		#Decrease health.
		health -= damageDealt
		if(!isAttacking || !isJumping):
			#Knocks back.
			var newKnockback = knockbackForce - knockbackResistance
			newKnockback.x = max(newKnockback.x, 0)
			newKnockback.y = max(newKnockback.y, 0)
			knockback(newKnockback, newDirection)
		#Checks if dead.
		if(health < 0.0):
			GameState.givePlayerExp(xpAmount)
			GameState.on_slime_killed()
			get_parent().mobCount -= 1
			get_parent().remove_child(self)
			queue_free()
#Invincibility timer.
func invincibilityTimer(delta):
	if(invincible):
		invincibleTime += delta
		if(invincibleTime >= INVINCIBLE_LIMIT):
			isDamageDealt = false
			invincibleTime = 0.0
			invincible = false
#Decision timer.
func decisionTimer(delta):
	#Starts timer.
	timer += delta
	#If time to make a decision.
	if(timer >= delay):
		#Resets timer.
		timer = 0
		#Randomizing the delay and decision.
		delay = MobBank.decisionDelay + rand_range(-MobBank.delayRandomizer, MobBank.delayRandomizer)
		decision = randomizeDecision()
func flipToDirection():
	set_global_transform(Transform2D(Vector2(-direction, 0), Vector2(0, 1), Vector2(global_position.x, global_position.y)))
func attack(enableDamage, reset=false):
	isAttacking = !reset
	canDamage = enableDamage
#Checks for collisions with the Player to damage it.
func checkCollisions():
	#Search for collisions.
	if(get_node("TouchDamageArea").get_overlapping_bodies().size() != 0):
		#If there is a collision.
		for body in get_node("TouchDamageArea").get_overlapping_bodies():
			#If collided body is Player.
			if(body.get_name() == "Player"):
				#Damages Player.
				if(!isAttacking): get_node("AnimationPlayer").play("Attack")
				#Damages Player.
				if(canDamage): body.damagePlayer(damage, knockbackForce, 0)
#Checks if Slime is going out of platform.
func checkCanWalkInDirection():
	var currentTiledPosition = getPositionInTiles()
	if(direction == -1 && GameState.MapProperties.currentForeground.get_cell(currentTiledPosition.x - 1, currentTiledPosition.y) == -1):
		decision = DecisionType.GO_RIGHT
		return false
	elif(direction == 1 && GameState.MapProperties.currentForeground.get_cell(currentTiledPosition.x + 1, currentTiledPosition.y) == -1):
		decision = DecisionType.GO_LEFT
		return false
	return true
#Checks collision with ground to reset vars.
func checkOnGround(delta):
	if(collision == null || collision.normal.y == 0): 
		velocity.y += GlobalVariables.GRAVITY * delta
	else: 
		velocity.y = 0
		isJumping = false
func isPlayerInRange():
	playerPos = player.global_position
	if(playerPos.y > global_position.y + spriteSize.y || playerPos.y + player.spriteHeight < global_position.y):
		return false
	return true
#Returns a Vector2 in tiles.
func getPositionInTiles():
	var directionX = 0
	if(direction == -1):
		directionX = global_position.x - shapeMargin
	elif(direction == 1):
		directionX = global_position.x + shapeMargin
	return Vector2(floor(directionX / GlobalVariables.TILE_SIZE), floor((global_position.y + 1 + (spriteSize.y / 2)) / GlobalVariables.TILE_SIZE))
#==============================================BEHAVIORS========================================================
	#Behavior type Passive.
func behavePassive():
	#If IDLE.
	if(decision == DecisionType.IDLE):
		#Resets velocity.
		velocity.x = 0
	#If GO_RIGHT or GO_LEFT.
	elif(decision == DecisionType.GO_RIGHT || decision == DecisionType.GO_LEFT):
		#Sets velocity.
		velocity.x = speed * decision
		direction = decision
		flipToDirection()
#Behavior type Follow.
func behaveFollow():
	#Gets the global_pos of Player.
	playerPos = get_tree().get_root().get_node("PlayState/Player").global_position
	#If the player in in the walkBounds follow him.
	if(isPlayerInRange() && checkCanWalkInDirection()):
		var playerSide = 1 if playerPos.x > global_position.x else -1
		velocity.x = speed * playerSide
		direction = playerSide
		flipToDirection()
	#If not behave normally.
	else:
		behavePassive()
#Behavior type Jumping.
func behaveJumping(delta):
	#Checks time between jumps.
	if(timeSinceLastJump >= TIME_BETWEEN_JUMPS):
		timeSinceLastJump = 0.0
		canJump = true
	if(!canJump): 
		timeSinceLastJump += delta
		velocity.x += DECELERATION * delta * -direction
	#Checks if Player is in the jumping area.
	if(get_node("JumpingArea").get_overlapping_bodies().size() > 0):
		for body in get_node("JumpingArea").get_overlapping_bodies():
			if(body.get_name() == "Player" && canJump):
				#Jumps toward the Player.
				velocity = Vector2(jumpForce.x * direction, -jumpForce.y)
				canJump = false
				isJumping = true