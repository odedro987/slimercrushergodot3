extends Node2D

var rect
var mobs
var spawnLevels
var mobCount
var maxMobs
var basicDelay
var timer
var delayRandomizer
var delay

var emitter
var toSpawn
var spawned

func _ready():
	randomize()
	#Init vars.
	rect = Rect2(0, 0, 0, 0)
	mobs = []
	spawnLevels = []
	mobCount = 0
	maxMobs = 0
	basicDelay = 0
	delay = 0
	timer = 0
	delayRandomizer = 0
	emitter = get_node("MobSpawnEmitter")
	toSpawn = null
	spawned = true
	#Update function.
	set_process(true)
#Set MobSpawner properties by global pos.
func setProperties(rect, mobs, spawnLevels, maxMobs, basicDelay=5, delayRandomizer=2):
	self.rect = rect
	self.mobs = mobs
	self.spawnLevels = spawnLevels
	self.maxMobs = maxMobs
	self.delayRandomizer = delayRandomizer
	self.basicDelay = basicDelay
	self.delay = basicDelay + rand_range(-delayRandomizer, delayRandomizer)
	scale = Vector2(1, 1)
#Set MobSpawner properties by global tiles.
func setPropertiesInTiles(rect, mobs, spawnLevels, maxMobs, basicDelay=5, delayRandomizer=2):
	setProperties(rect, mobs, spawnLevels, maxMobs, basicDelay, delayRandomizer)
	self.rect = Rect2(rect.position.x * GlobalVariables.TILE_SIZE, rect.position.y * GlobalVariables.TILE_SIZE, rect.size.x * GlobalVariables.TILE_SIZE, rect.size.y * GlobalVariables.TILE_SIZE)
#Set MobSpawner properties by scale.
func setPropertiesFromScale(mobs, spawnLevels, maxMobs, basicDelay=5, delayRandomizer=2):
	setPropertiesInTiles(getRectFromScale(), mobs, spawnLevels, maxMobs, basicDelay, delayRandomizer)
func getRectFromScale():
	var sizeTiles = Vector2(GlobalVariables.NODE_SIZE * scale.x, 1)
	var positionTiles = Vector2((global_position.x - (sizeTiles.x / 2)) / GlobalVariables.TILE_SIZE, global_position.y / GlobalVariables.TILE_SIZE)
	return Rect2(positionTiles.x, positionTiles.y, sizeTiles.x / GlobalVariables.TILE_SIZE, sizeTiles.y)
func _process(delta):
	#Spawns mob after done emitting.
	if(!spawned && !emitter.emitting):
		#Fade in spawn.
		add_child(toSpawn)
		toSpawn.get_node("AnimationPlayer").play("FadeInSpawn")
		#Resets mob position.
		toSpawn.global_position = emitter.global_position
		spawned = true
	#Increment timer if mobs aren't maxed.
	if(mobCount < maxMobs):
		timer += delta
	#If ready to spawn.
	if(timer >= delay && spawned):
		spawnMob("random")
func spawnMob(mob):
	emitter.restart()
	#Spawns a random mob.
	var mobToSpawn = generateMobToSpawn() if mob == "random" else mob
	toSpawn = MobBank.getMobByName(mobToSpawn)
	toSpawn.setType(mobToSpawn)
	#Gets height and width of the mob.
	var toSpsawnSize = toSpawn.get_node("Sprite").texture.get_size()
	var heightDiff = toSpsawnSize.y / toSpawn.get_node("Sprite").get_vframes() / 2
	var widthDiff  = toSpsawnSize.x / toSpawn.get_node("Sprite").get_hframes() / 2
	#Randomizes the position of the mob and the emitter.
	toSpawn.global_position = Vector2(rand_range(rect.position.x, rect.end.x), rect.position.y - heightDiff)
	emitter.global_position = toSpawn.global_position
	#Starts particle emitter.
	emitter.emitting = true
	spawned = false
	#Increment mobCount.
	mobCount = mobCount + 1
	#Reset timer.
	timer = 0
	#Randomize delay.
	delay = basicDelay + rand_range(-delayRandomizer, delayRandomizer) - GameState.Difficulty.spawnRateMultiplier
func generateMobToSpawn():
	var mobToSpawn = floor(rand_range(0, mobs.size()))
	if(spawnLevels[mobToSpawn] > GameState.Difficulty.maxSpawnLevel): return generateMobToSpawn()
	elif(spawnLevels[mobToSpawn] < GameState.Difficulty.minSpawnLevel): return generateMobToSpawn()
	else: return mobs[mobToSpawn]