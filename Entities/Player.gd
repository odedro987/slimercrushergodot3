extends KinematicBody2D

#Running max speed.
const MAX_SPEED = 150
#Roll velocity.
const ROLL_VELOCITY = 225
#Acceleration.
const ACCELERATION = 800
#Deceleration when stop moving.
const DECELERATION = 800
#Deceleration when changing directions.
const DIRECTION_MOMENTUM_FORCE = DECELERATION
#Charge jump factor.
const CHARGE_JUMP_FACTOR = 0.4
#Small jump speed.
const SMALL_JUMP_SPEED = 200
#Time between jumps.
const JUMP_INTERVAL_LIMIT = 0.15
#Time until charge jump ends.
const MAX_CHARGE_JUMP_TIME = 0.2
#Time for hit-invincibility.
const INVINCIBLE_LIMIT = 0.75
#Time for roll-invincibility.
const INVINCIBLE_ROLL_LIMIT = 0.75
#Time until movement is enabled after knockback.
const KNOCKBACK_MOVEMENT_LIMIT = 0.2
#Flickering rate on hurt.
const HURT_FLICKER_RATE = 4
#Attack animations.
const ATTACK_ANIMATION_TYPES = ["Thrust", "Slash"]
#The offset of the sprite at idle to frame size.
const PLAYER_IDLE_OFFSET = 4
#Small fraction to ignore slight push up from colliding with floor.
const ON_GROUND_Y_OFFSET = 0.1

###########MOVEMENT VARIABLES###########
var velocity = Vector2(0, 0)
var acceleration = 0
var motion
var normalize
var direction = 1
var canJump = true
var canMove = true
var collision
var isRolling = false
var rollRecover = false
#Jumps
var onGround = false
var jumpChargeTime = 0
var chargingJump = false
var timeSinceLastDrop = 0.0
############DAMAGE VARIABLES############
var health = GameState.Player.MaxHealth
var isDamageDealt = false
var invincibleTime = 0.0
var invincible = false
var invincibleLimit = INVINCIBLE_LIMIT
var isAttacking = false
var canDamage = false
#############OTHER VARIABLES############
enum States { NORMAL, HURT, ATTACK, ROLL, DISABLED }
var currentState 
var currentAnimation = "None"
var sprite
var spriteHeight
var animationPlayer
var weaponSprite
var attackCollisionArea
#############INPUT VARIABLES############
var pressedLeft
var pressedRight
var pressedUp
var pressedDown
var pressedSpace
var pressedShift

func _ready():
	initNodeVariables()
	currentState = States.NORMAL
	spriteHeight = sprite.texture.get_size().y / sprite.vframes
	set_physics_process(true)
func _physics_process(delta):
	invincibilityTimer(delta)
	if(currentState == States.NORMAL):
		checkInput()
		rollInput(delta)
		movementInput(delta)
		jumpInput(delta)
		attackInput(delta)
		applyGravity(delta)
	elif(currentState == States.ATTACK):
		checkInput()
		attackInput(delta)
		applyGravity(delta)
		velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -(velocity.x / abs(velocity.x))
	elif(currentState == States.HURT):
		damageInput(delta)
		applyGravity(delta)
		#Calcualtes the direction of the knockback force(the sign +/-) and decelerates to the opposite direction of the hit.
		if(velocity.x != 0):
			var hitDirection = velocity.x / abs(velocity.x)
			velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -hitDirection
	elif(currentState == States.ROLL):
		checkInput()
		rollInput(delta)
		applyGravity(delta)
	elif(currentState == States.DISABLED):
		#Return to idle state.
		if(currentAnimation != "Idle"): 
			playAnimation("Idle")
		applyGravity(delta)
		velocity.x = 0
	motion = velocity * delta
	collision = move_and_collide(motion)
func movementInput(delta):
	#Reseting acceleration.
	acceleration = 0
	#Checking if pressed movement buttons.
	if((pressedLeft || pressedRight) && canMove):
		#Setting direction.
		direction = -1 if pressedLeft else 1
		#Accelerating.
		acceleration = ACCELERATION * direction
		#As long as Walk animation isn't playing play Walk animation. 
		if(notPlayingAnimations(["Walk", "Jump", "Land", "Fall", "Attack", "Roll"])):
			playAnimation("Walk")
		#Decelerating if suddenly changes direction.
		if(velocity.x != 0 && (velocity.x / abs(velocity.x)) != direction): 
			velocity.x = 0 if abs(velocity.x) + DIRECTION_MOMENTUM_FORCE * direction * delta < 0 else velocity.x + DIRECTION_MOMENTUM_FORCE * direction * delta
		#Flips the sprites and attack collision area.
		set_global_transform(Transform2D(Vector2(direction, 0), Vector2(0, 1), Vector2(position.x, position.y)))
	else:
		#As long as Idle animation isn't playing play Idle animation. 
		if(notPlayingAnimations(["Idle", "Jump", "Land", "Fall", "Attack", "Roll"])):
			playAnimation("Idle")
		#Decelerating and sliding(according to velocity and not sprite direction).
		velocity.x = 0 if abs(velocity.x) - DECELERATION * delta < 0 else velocity.x + DECELERATION * delta * -(velocity.x / abs(velocity.x))
	#Setting velocity to acceleration time delta.
	velocity.x = velocity.x + acceleration * delta if abs(velocity.x) < MAX_SPEED else MAX_SPEED * direction
func rollInput(delta):
	if(pressedShift && notPlayingAnimations(["Roll"])):
		playAnimation("Roll")
		currentState = States.ROLL
	if(isRolling && !rollRecover):
		velocity.x = ROLL_VELOCITY * direction
func jumpInput(delta):
	checkOnGround()
	checkIfFalling(delta)
	#Increment timeSinceLastDrop.
	timeSinceLastDrop = timeSinceLastDrop + delta if timeSinceLastDrop < 1 else 0.5
	#If pressed down and on ground.
	if(pressedDown && onGround && canDropJump()):
		#Disable collision mask to fall through platform.
		set_collision_mask_bit(2, false)
		timeSinceLastDrop = 0.0
	#Reset when done.
	if timeSinceLastDrop > 0.1:
		set_collision_mask_bit(2, true)
	#If pressed up.
	if(pressedUp && canMove):
		#If player is on ground.
		if(onGround):
			playAnimation("Jump")
			chargingJump = true
	#If up key is released reset chargingJump.
	else:
		chargingJump = false
		jumpChargeTime = 0
	#If chargingJump.
	if(chargingJump):
		#Starts timer.
		jumpChargeTime += delta
		#Adds a fraction of small speed jump to the small jump.
		velocity.y = -SMALL_JUMP_SPEED - (SMALL_JUMP_SPEED * jumpChargeTime * CHARGE_JUMP_FACTOR)
		#if timer is up, reset it.
		if(jumpChargeTime >= MAX_CHARGE_JUMP_TIME): 
			chargingJump = false
			jumpChargeTime = 0
func attackInput(delta):
	if(pressedSpace && !isAttacking):
		currentState = States.ATTACK
		currentAnimation = "Attack"
		animationPlayer.play(ATTACK_ANIMATION_TYPES[rand_range(0, ATTACK_ANIMATION_TYPES.size())])
		animationPlayer.playback_speed = GameState.Player.AttackSpeed
		isAttacking = true
		invincibleLimit = INVINCIBLE_LIMIT
		chargingJump = false
	#Search for collisions.
	if(attackCollisionArea.get_overlapping_bodies().size() != 0):
		#If there is a collision.
		for body in attackCollisionArea.get_overlapping_bodies():
			#If collided body is Player.
			if(("Slime" in body.name) && isAttacking && canDamage):
				#Damages Player.
				body.damageEnemy(GameState.Player.AttackDamage, Vector2(80, 130), direction)
#Decreases health.
func damagePlayer(damageDealt, knockbackForce, newDirection):
	if(!invincible && currentState != States.ROLL):
		damageDealt -= GameState.Player.Armor
		if damageDealt < 1:
			damageDealt = 1
		#Sets invincible and isDamageDealt to true.
		invincible = true
		isDamageDealt = true
		#Disable movement and reset jumping and attack.
		canMove = false
		chargingJump = false
		isAttacking = false
		#Play animation.
		animationPlayer.play("Idle")
		currentAnimation = "Idle"
		#Decrease health.
		var oldHealth = health
		health -= damageDealt
		if(health < 0.0):
			GameState.on_player_death(self)
			GameState.Stats.totalSlimesKilled += GameState.Stats.currentSlimesKilled
			GameState.Stats.currentSlimesKilled = 0
			health = 0
		GameState.on_player_health_change(self, oldHealth, health)
		#Knocks back.
		knockback(knockbackForce, newDirection)
		#Change state to HURT.
		currentState = States.HURT
#Damage checks.
func damageInput(delta):
	#Checks if damage is dealt, if so start hit-invincibility.
	if(isDamageDealt):
		#Starts the invincibility counter.1
		modulate.a = modulate.a + delta * HURT_FLICKER_RATE if modulate.a < 1 else 0.2
		#If counter is over.
		if(invincibleTime >= KNOCKBACK_MOVEMENT_LIMIT):
			if(!canMove): canMove = true
			currentState = States.NORMAL
#Applies gravity.
func applyGravity(delta):
	if(collision == null || collision.normal.y == 0): 
		velocity.y += GlobalVariables.GRAVITY * delta
	else: velocity.y = 0
#Checks if player in onGround.
func checkOnGround():
	onGround = (collision != null && collision.normal.y == -1)
	#If onGround and should Idle.
	if(onGround && notPlayingAnimations(["Idle", "Walk", "Land", "Attack", "Roll"])):
		playAnimation("Idle")
#Checks if player if falling.
func checkIfFalling(delta):
	if(!onGround):
		#If falling, meaning current position is lower than your position in the next frame(current position + velocity.y).
		if(global_position.y + ON_GROUND_Y_OFFSET < global_position.y + velocity.y * delta):
			if(notPlayingAnimations(["Fall", "Attack", "Roll"])):
				playAnimation("Fall")
#Applies knockback.
func knockback(force, newDirection):
	acceleration = 0
	velocity = Vector2(0, 0)
	newDirection = newDirection if newDirection != 0 else -direction
	velocity = Vector2(force.x * newDirection, -force.y)
	canMove = false
#Resets isAttacking.
func resetIsAttacking():
	isAttacking = false
	canDamage = false
	currentState = States.NORMAL
	currentAnimation = "None"
	animationPlayer.playback_speed = 1.0
#Enable damaging enemies when correct attack frame.
func enableDamage():
	canDamage = true
#Invincibility timer
func invincibilityTimer(delta):
	if(invincible):
		invincibleTime += delta
		if(invincibleTime >= invincibleLimit):
			#Resets variables.
			isDamageDealt = false
			invincibleTime = 0.0
			invincible = false
			modulate.a = 1.0
func roll(animationFlag, invincibilityFlag=false):
	isRolling = animationFlag
	canMove = !animationFlag
	rollRecover = !animationFlag
	if(animationFlag): 
		animationPlayer.playback_speed = 1.5
		get_node("Weapon").hide()
	if(invincibilityFlag):
		animationPlayer.playback_speed = 1.0
		currentAnimation = "Idle"
		currentState = States.NORMAL
		get_node("Weapon").show()
#Plays aniamtion and set currentAnimation.
func playAnimation(animationName):
	animationPlayer.play(animationName)
	currentAnimation = animationName
#Checks if currentAnimation isn't on from the array sent.
func notPlayingAnimations(var animations):
	for anim in animations: 
		if (currentAnimation == anim): return false
	return true
#Returns whether or not the player can jump down a platform.
func canDropJump():
	#Checks if the player position + his height is less the ground level.
	return global_position.y + (sprite.texture.get_height() / sprite.vframes - PLAYER_IDLE_OFFSET) < GameState.MapProperties.groundLevel
func initNodeVariables():
	sprite = get_node("Sprite")
	animationPlayer = get_node("AnimationPlayer")
	attackCollisionArea = get_node("AttackCollisionArea")
func checkInput():
	pressedLeft = Input.is_action_pressed("ui_left")
	pressedRight = Input.is_action_pressed("ui_right")
	pressedUp = Input.is_action_pressed("ui_up")
	pressedDown = Input.is_action_pressed("ui_down")
	pressedSpace = Input.is_action_pressed("ui_select")
	pressedShift = Input.is_action_pressed("roll")