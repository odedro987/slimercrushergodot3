extends Node

class PlayerStats:
	var AttackDamage = 10
	var AttackSpeed = 1.0
	var MaxHealth = 100
	var Armor = 0
	var XP = 0

class PlayerUpgradeStats:
	var AttackDamage = 0
	var AttackSpeed = 0
	var MaxHealth = 0
	var Armor = 0

class MapsProperties:
	var currentMapName = ""
	var groundLevel = 0
	var currentForeground = null

class Statistics:
	var currentSlimesKilled = 0
	var totalSlimesKilled = 0

class DifficultyStats:
	var spawnRateMultiplier = 0.5
	var maxSpawnMultiplier = 1
	var maxSpawnLevel = 1
	var minSpawnLevel = 1
	var mobHealthMultiplier = 1
	var mobSpeedMultiplier = 1
	var mobDamageMultiplier = 1
	var mobEXPMultiplier = 1

var Player = PlayerStats.new()
var PlayerUpgrades = PlayerUpgradeStats.new()
var MapProperties = MapsProperties.new()
var Difficulty = DifficultyStats.new()
var Stats = Statistics.new()

signal player_health_changed
signal player_exp_changed
signal slime_killed

func loadScene(file):
	get_tree().change_scene(file)

func loadShop():
	get_tree().change_scene("res://UI/Shop.tscn")
	
func on_player_death(player):
	loadShop()
	Stats.totalSlimesKilled += Stats.currentSlimesKilled
	
func on_player_health_change(player, oldHealth, newHealth):
	emit_signal("player_health_changed", oldHealth, newHealth)

func on_slime_killed():
	Stats.currentSlimesKilled += 1
	emit_signal("slime_killed", Stats.currentSlimesKilled)

func givePlayerExp(change):
	var oldExp = Player.XP
	var newExp = Player.XP + change
	Player.XP = newExp
	emit_signal("player_exp_changed", newExp)
	
func payExp(price):
	if Player.XP >= price:
		Player.XP -= price
		emit_signal("player_exp_changed", Player.XP)
		return true
	return false

func resetDifficulty():
	Difficulty.spawnRateMultiplier = 0.5
	Difficulty.maxSpawnMultiplier = 1
	Difficulty.maxSpawnLevel = 1
	Difficulty.minSpawnLevel = 1
	Difficulty.mobHealthMultiplier = 1
	Difficulty.mobSpeedMultiplier = 1
	Difficulty.mobDamageMultiplier = 1
	Difficulty.mobEXPMultiplier = 1