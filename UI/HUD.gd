extends CanvasLayer

func _ready():
	GameState.connect("player_health_changed", self, "update_health_bar")
	GameState.connect("player_exp_changed", self, "update_exp_label")
	
	update_health_bar(GameState.Player.MaxHealth, GameState.Player.MaxHealth)
	update_exp_label(GameState.Player.XP)

func update_health_bar(oldHealth, newHealth):
	get_node("RefFrame/MaxHealth").set_size(Vector2(GameState.Player.MaxHealth, 11))
	get_node("RefFrame/MaxHealth/Health").set_size(Vector2(newHealth, 11))

func update_exp_label(newExp):
	get_node("RefFrame/Sprite/ExpLabel").set_text(String(newExp))