extends Button

export var upgradeText = "+1" setget setUpgradeText
export var upgradeCost = 100 setget setUpgradeCost

func _ready():
	set_process(true)
	
func _process():
	set_disabled(GameState.Player.XP < upgradeCost)

func setUpgradeText(val):
	upgradeText = val
	if has_node("UpgradeLabel"):
		get_node("UpgradeLabel").set_text(String(val))
	
func setUpgradeCost(val):
	upgradeCost = val
	if has_node("CostLabel"):
		get_node("CostLabel").set_text(String(val))