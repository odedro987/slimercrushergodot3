extends Control

func _ready():
	GameState.connect("player_exp_changed", self, "updateExpLabel")
	updateExpLabel(GameState.Player.XP)

func startGame():
	GameState.loadScene("res://States/PlayState.tscn")
	
func updateExpLabel(newExp):
	get_node("Stats/Sprite/ExpLabel").set_text(":" + String(newExp))