extends Panel

export var stat = "AttackDamage"
export var statName = "Attack Damage"
export var statMultiplier = 1
export var statSuffix = " Damage"
export var icon = 0

export var basePrice = 100
export var priceMultiplier = 1.25
export var increasePerUpgrade = 10.0
export var upgradeValueIncrease = 1.0

func _ready():
	set_process(true)

func _process(delta):
	get_node("Icon").set_frame(icon)
	get_node("NameLabel").set_text(statName)
	get_node("ValueLabel").set_text(String(GameState.Player.get(stat) * statMultiplier) + statSuffix)
	get_node("StatUpgradeButton").setUpgradeText("+" + String(getUpgradeIncrease()))
	get_node("StatUpgradeButton").setUpgradeCost(getPrice())

func getPrice():
	return floor(basePrice * pow(priceMultiplier, GameState.PlayerUpgrades.get(stat)))
	
func getUpgradeIncrease():
	return increasePerUpgrade + GameState.PlayerUpgrades.get(stat) * upgradeValueIncrease
	
func buyUpgrade():
	if GameState.payExp(getPrice()):
		GameState.Player.set(stat, GameState.Player.get(stat) + getUpgradeIncrease())
		GameState.PlayerUpgrades.set(stat, GameState.PlayerUpgrades.get(stat) + 1)