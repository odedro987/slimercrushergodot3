extends Node

func getMapGroundLevelByName(name):
	var groundLevelInTiles = 0
	if(name == "Forest1"): groundLevelInTiles = 17
	elif(name == "Forest2"): groundLevelInTiles = 17
	else: groundLevelInTiles = -1
	return groundLevelInTiles * GlobalVariables.TILE_SIZE

func getMapByIndex(index):
	if(index < get_children().size()):
		GameState.MapProperties.currentMapName = get_children()[index].name
		return get_children()[index]
	return null

func getMapByName(name):
	for map in get_children():
		if(map.name == name):
			GameState.MapProperties.currentMapName = map.name
			return map
	return null