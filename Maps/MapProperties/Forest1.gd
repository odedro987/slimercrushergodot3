extends Node2D

func _ready():
	get_node("Spawners/MobSpawner").setPropertiesFromScale(["SlimePink", "SlimeBlue", "SlimeRed"], [1, 2, 3], 1)
	get_node("Spawners/MobSpawner2").setPropertiesFromScale(["SlimeMini", "SlimePink", "SlimeBlue"], [1, 1, 2], 1)
	get_node("Spawners/MobSpawner3").setPropertiesFromScale(["SlimeMini", "SlimePink", "SlimeBlue", "SlimeRed"], [1, 1, 1, 2], 5)
	get_node("Spawners/MobSpawner4").setPropertiesFromScale(["SlimeMini", "SlimePink", "SlimeBlue"], [2, 1, 2], 2)
	get_node("Spawners/MobSpawner5").setPropertiesFromScale(["SlimeMini", "SlimePink", "SlimeBlue"], [1, 1, 2], 2)
