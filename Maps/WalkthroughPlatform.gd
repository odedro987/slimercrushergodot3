extends StaticBody2D

func _ready():
	#Creates a new RectangleShape2D in size of the editor-scaled StaticBody2D.
	var rect = RectangleShape2D.new()
	rect.set_extents(Vector2(get_scale().x * GlobalVariables.NODE_SIZE / 2, get_scale().y * GlobalVariables.NODE_SIZE / 2))
	#Resets scale.
	set_scale(Vector2(1, 1))
	#Loads new shape.
	get_node("CollisionShape2D").shape = rect