extends Node

var maps = preload("res://Maps/Maps.tscn").instance()
var currentMap = 0
var map = null
var camera = null
var timeElapsed = 0

func _ready():
	randomize()
	initNodeVariables()
	set_process(true)
	loadMap(currentMap)
	camera.setFollowingNode(get_node("Player"))
	camera.set_zoom(Vector2(1.0 / GlobalVariables.ZOOM, 1.0 / GlobalVariables.ZOOM))
	#Sets the camera's limits to the cuurent map size.
	var tilemapRect = map.get_child(0).get_node("Foreground").get_used_rect()
	print(tilemapRect.position.x)
	camera.setAllLimits(tilemapRect.position.x * GlobalVariables.TILE_SIZE, -(tilemapRect.position.y + 15) * GlobalVariables.TILE_SIZE,
												tilemapRect.end.x * GlobalVariables.TILE_SIZE, tilemapRect.end.y * GlobalVariables.TILE_SIZE)
	get_node("DifficultyPlayer").play("Difficulty")
	GameState.connect("slime_killed", self, "checkSpawnSpecial")

func loadMap(mapIndex):
	if(map.get_child_count() > 0): 
		map.remove_child(map.get_child(0))
	map.add_child(maps.getMapByIndex(mapIndex).duplicate())
	map.get_child(0).show()
	GameState.MapProperties.currentMapName = map.get_child(0).name
	GameState.MapProperties.groundLevel = maps.getMapGroundLevelByName(GameState.MapProperties.currentMapName)
	GameState.MapProperties.currentForeground = map.get_child(0).get_node("Foreground")
	GameState.resetDifficulty()

func _process(delta):
	if(Input.is_action_just_released("ui_focus_next")):
		currentMap = 1 if currentMap == 0 else 0
		loadMap(currentMap)
	timeElapsed += delta
	get_node("HUD/TimeLabel").text = "Time Elapsed: " + str(int(timeElapsed / 60)) + ":" + str(int(timeElapsed) % 60)

func initNodeVariables():
	map = get_node("Map")
	camera = get_node("Player/Camera2DFX")

func checkSpawnSpecial(slimesKilled):
	if(slimesKilled % 10 == 0): 
		var spawners = get_node("Map/" + get_node("Map").get_child(0).get_name() + "/Spawners")
		spawners.get_child(int(rand_range(0, spawners.get_child_count()))).spawnMob("SlimeBig")

func raiseDifficulty(raiseMinimumSpawnLevel=false, buffMobs=false):
	GameState.Difficulty.spawnRateMultiplier *= 1.5
	GameState.Difficulty.maxSpawnMultiplier  += 0.1
	GameState.Difficulty.maxSpawnLevel       += 1.0
	if(raiseMinimumSpawnLevel): GameState.Difficulty.minSpawnLevel += 1.0
	if(buffMobs):
		GameState.Difficulty.mobDamageMultiplier *= 1.5
		GameState.Difficulty.mobSpeedMultiplier  *= 1.5
		GameState.Difficulty.mobHealthMultiplier *= 1.5
		GameState.Difficulty.mobEXPMultiplier    *= 1.5
	for spawner in get_node("Map/" + get_node("Map").get_child(0).get_name() + "/Spawners").get_children():
		spawner.maxMobs *= GameState.Difficulty.maxSpawnMultiplier